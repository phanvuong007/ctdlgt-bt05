package ctgl.bt05;


public class DoubleCoca {
    Node first, last;
    public class Node {
        String item;
        Node next;
    }
    public boolean isEmpty() {
        return first == null;
    }
    
    public void enqueue(String item) {
        Node old = last;
        last = new Node();
        last.item = item;
        last.next = null;
        if(isEmpty()) first = last;
        else {
            old.next = last;
        }
    }
    
    public String dequeue() {
        String item = first.item;
        first = first.next;
        if(isEmpty()) last = null;
        return item;        
    }
    
    public static void main(String[] args) {
        DoubleCoca dc = new DoubleCoca();
        String [] s = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};
        int n = 3;
        int i = 0;
        dc.enqueue(s[0]);
        dc.enqueue(s[1]);
        dc.enqueue(s[2]);
        dc.enqueue(s[3]);
        dc.enqueue(s[4]);
        String s1 = null;
        while(i < n) {
            s1 = dc.dequeue();
            dc.enqueue(s1);
            dc.enqueue(s1);
            i++;
            if(i == n - 1) {
                s1 = dc.dequeue();
                break;
            }
        }
        System.out.println(s1);
    }
}