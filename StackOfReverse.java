package ctgl.bt05;


public class StackOfReverse {

    private Node first = null;

    private class Node {

        int item;
        Node next;
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void push(int item) {
        Node old = first;
        first = new Node();
        first.item = item;
        first.next = old;
    }

    public int pop() {
        int item = first.item;
        first = first.next;
        return item;
    }

    public static void main(String[] args) {
        StackOfReverse st = new StackOfReverse();
        String s1 = "5 1 2 + 4 * + 3 -";

        char[] s = s1.toCharArray();
        int i = 0;
        while (i < s.length) {
            if (s[i] == ' ' || s[i] == '+' || s[i] == '*' || s[i] == '-') {
                if (s[i] == '+') {
                    int tong;
                    tong = st.pop();
                    tong = st.pop() + tong;
                    st.push(tong);

                      } 
                else if (s[i] == '-') {
                    int tong;
                    tong = st.pop();
                    tong = st.pop() - tong;
                    st.push(tong);

                      } 
                else if (s[i] == '*') {
                    int tong;
                    tong = st.pop();
                    tong = st.pop() * tong;
                    st.push(tong);

                      } 
                else if (s[i] == '/') {
                    int tong;
                    tong = st.pop();
                    tong = st.pop() + tong;
                    st.push(tong);

                      } 
            } else if (s[i] != '+' || s[i] != '-' || s[i] != '*' || s[i] != '/') {
                int k = (int) s[i] - 48;
                st.push(k);

            }

            i++;

        }
        System.out.println(st.pop());
        //System.out.println(st.pop());

    }
}